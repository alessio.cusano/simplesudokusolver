#include "NumberSet.h"
#include <iostream>

NumberSet::NumberSet(int min, int max) :
	m_Min(min),
	m_Max(max),
	m_Digits(std::vector<int>(0))
{
}

NumberSet::NumberSet() :
	m_Min(1),
	m_Max(9),
	m_Digits(std::vector<int>(0))
{
}

NumberSet::NumberSet(int min, int max, std::vector<int> digits) :
	m_Min(min),
	m_Max(max),
	m_Digits(digits)
{
}

NumberSet::NumberSet(const NumberSet& other)
{
	m_Min = other.m_Min;
	m_Max = other.m_Max;
	m_Digits = other.m_Digits;
}

void NumberSet::add(int digit)
{
	if (!contains(digit) && digit != 0) m_Digits.push_back(digit);
}

void NumberSet::remove(int digit)
{
	int idx = indexOf(digit);
	if(idx != -1) m_Digits.erase(m_Digits.begin() + idx);
}

bool NumberSet::contains(int digit) const
{
	return indexOf(digit) != -1;
}

int NumberSet::indexOf(int digit) const
{
	for (int i = 0; i < m_Digits.size(); i++) {
		if (m_Digits[i] == digit) return i;
	}
	return -1;
}

NumberSet NumberSet::getSpecular() const
{
	NumberSet values(m_Min, m_Max);

	for (int i = m_Min; i <= m_Max; i++) {
		if (!contains(i)) values.add(i);
	}

	return values;
}

std::string NumberSet::print() const
{
	std::string result = "|";

	for (int i = 0; i < m_Digits.size(); i++) {
		result += std::to_string(m_Digits[i]) + " ";
	}

	return result + "|";
}

int NumberSet::size() const
{
	return m_Digits.size();
}

int& NumberSet::operator[](int idx)
{
	return m_Digits[idx];
}

const int& NumberSet::operator[](int idx) const
{
	return m_Digits[idx];
}

NumberSet NumberSet::operator+(const NumberSet& other)
{
	NumberSet value(*this);

	for (int i = 0; i < other.size(); i++) {
		int val = other[i];
		value.add(val);
	}

	return value;
}
