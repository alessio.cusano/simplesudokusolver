#pragma once
#include <vector>
#include <string>

class Utils
{
public:
	static std::vector<std::string> split(std::string s, char delimiter);
};

