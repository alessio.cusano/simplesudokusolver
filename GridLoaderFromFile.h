#pragma once
#include <vector>
#include <string>
#include <fstream>
#include <iostream>

class GridLoaderFromFile
{
private:
	std::string m_Filename;
	std::vector<std::string> m_Lines;

public:
	GridLoaderFromFile(std::string filename);
	void read();
	const std::vector<std::string>& getLines() const;
};
