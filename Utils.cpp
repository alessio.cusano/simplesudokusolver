#include "Utils.h"

std::vector<std::string> Utils::split(std::string s, char delimiter)
{
    std::vector<std::string> result;
    int start = 0;
    int end = s.find(delimiter);
    while (end != -1) {
        result.push_back(s.substr(start, end - start));
        start = end + 1;
        end = s.find(delimiter, start);
    }
    result.push_back(s.substr(start, end - start));

    return result;
}
