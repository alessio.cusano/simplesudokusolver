#include "GridLoaderFromFile.h"

GridLoaderFromFile::GridLoaderFromFile(std::string filename) : m_Filename(filename)
{
    read();
}

void GridLoaderFromFile::read()
{
    std::string line;
    std::ifstream myfile(m_Filename);

    m_Lines.clear();

    if (myfile.is_open())
    {
        while (getline(myfile, line))
        {
            m_Lines.push_back(line);
        }
        myfile.close();
    }

    else std::cout << "Unable to open file";
}

const std::vector<std::string>& GridLoaderFromFile::getLines() const
{
    return m_Lines;
}
