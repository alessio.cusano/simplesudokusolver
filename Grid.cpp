#include "Grid.h"

Grid::Grid() : m_RemainigDigits(ROWS * COLS)
{
	for (int row = 0; row < ROWS; row++) {
		m_Grid.push_back(std::vector<int>(COLS));
	}
}

void Grid::set(int position, int value)
{
	int row = getRowByPosition(position);
	int col = getColByPosition(position);
	set(row, col, value);
}

void Grid::set(int row, int col, int value)
{
	if(value != 0) m_RemainigDigits--;
	m_Grid[row][col] = value;
}

int Grid::get(int position)
{
	int row = getRowByPosition(position);
	int col = getColByPosition(position);
	return get(row, col);
}

int Grid::get(int row, int col)
{
	return m_Grid[row][col];
}

void Grid::setRemainingDigits(int value)
{
	m_RemainigDigits = value;
}

int Grid::getRemainingDigits() const
{
	return m_RemainigDigits;
}

NumberSet Grid::getRow(int row) const
{
	NumberSet values;

	for (int col = 0; col < COLS; col++) {
		if(m_Grid[row][col] != 0) values.add(m_Grid[row][col]);
	}

	return values;
}

NumberSet Grid::getCol(int col) const
{
	NumberSet values;

	for (int row = 0; row < ROWS; row++) {
		if (m_Grid[row][col] != 0) values.add(m_Grid[row][col]);
	}

	return values;
}

NumberSet Grid::getRegion(int row, int col) const
{
	NumberSet region;

	int startRow = (row / REGION_ROWS) * REGION_ROWS;
	int startCol = (col / REGION_COLS) * REGION_COLS;

	for (int r = startRow; r < startRow + REGION_ROWS; r++) {
		for (int c = startCol; c < startCol + REGION_COLS; c++) {
			region.add(m_Grid[r][c]);
		}
	}

	return region;
}

std::string Grid::print() const
{
	std::string result = "";

	int value = 0;
	for (int row = 0; row < ROWS; row++) {
		for (int col = 0; col < COLS; col++) {
			value = m_Grid[row][col];
			result += (value != 0 ? std::to_string(value) : " ") + " ";
			if((col + 1) % 3 == 0 && col + 1 < COLS) result += "| ";
		}
		result += "\n";
		if ((row + 1) % 3 == 0 && row + 1 < ROWS) result += "----------------------\n";
	}

	return result;
}

int Grid::getRowByPosition(int position) const
{
	return position / ROWS;
}

int Grid::getColByPosition(int position) const
{
	return position % COLS;
}
