#pragma once
#include <vector>
#include <string>
#include <iostream>
#include "NumberSet.h"

#define ROWS 9
#define COLS 9

#define REGION_ROWS 3
#define REGION_COLS 3

class Grid
{
private:
	std::vector<std::vector<int>> m_Grid;
	int m_RemainigDigits = 0;

public:
	Grid();
	void set(int position, int value);
	void set(int row, int col, int value);
	int get(int position);
	int get(int row, int col);
	void setRemainingDigits(int value);
	int getRemainingDigits() const;
	NumberSet getRow(int row) const;
	NumberSet getCol(int col) const;
	NumberSet getRegion(int row, int col) const;
	std::string print() const;


private:
	int getRowByPosition(int position) const;
	int getColByPosition(int position) const;
};

