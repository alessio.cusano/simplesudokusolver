#include <iostream>
#include "GridLoaderFromFile.h"
#include <map>
#include <algorithm>
#include "Utils.h"
#include "Grid.h"
#include "NumberSet.h"

#include <chrono>
using namespace std::chrono;

void readSudoku(GridLoaderFromFile& gridLoader, Grid& grid);
void solve(Grid& grid);
void removeValueFromRows(int c, int value, std::map<int, NumberSet>& possibleValues);
void removeValueFromCols(int r, int value, std::map<int, NumberSet>& possibleValues);
void removeValueFromRegion(int r, int c, int value, std::map<int, NumberSet>& possibleValues);

int main()
{
    GridLoaderFromFile gridLoader("Resources/input_easy.txt");
    //GridLoaderFromFile gridLoader("Resources/input_hard.txt");
    Grid grid;

    readSudoku(gridLoader, grid);

    std::cout << "Empty: " << grid.getRemainingDigits() << std::endl;
    std::cout << grid.print() << std::endl;

    auto start = high_resolution_clock::now();

    solve(grid);

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);

    std::cout << "Empty: " << grid.getRemainingDigits() << std::endl;
    std::cout << grid.print() << std::endl;

    std::cout << "Time taken by function: " << float(duration.count()) / 1000000 << " seconds" << std::endl;
}

void readSudoku(GridLoaderFromFile& gridLoader, Grid& grid) {
    std::vector<std::string> lines = gridLoader.getLines();
    std::vector<std::string> digits;
    std::string line;
    int value = 0;
    for (int row = 0; row < lines.size(); row++) {
        line = lines[row];

        std::replace(line.begin(), line.end(), ';', ',');
        digits = Utils::split(line, ',');

        for (int col = 0; col < digits.size(); col++) {
            if (digits[col] == " ") digits[col] = "0";

            value = stoi(digits[col]);
            grid.set(row, col, value);
        }
    }
}

void solve(Grid& grid) {
    std::map<int, NumberSet> possibleValues;

    for (int r = 0; r < ROWS; r++) {
        for (int c = 0; c < COLS; c++) {

            if (grid.get(r, c) != 0) continue;

            possibleValues[c + r * COLS] = (grid.getRegion(r, c) + grid.getRow(r) + grid.getCol(c)).getSpecular();

            if (possibleValues[c + r * COLS].size() == 1) {
                int value = possibleValues[c + r * COLS][0];
                grid.set(r, c, value);

                removeValueFromRows(c, value, possibleValues);
                removeValueFromCols(r, value, possibleValues);
                removeValueFromRegion(r, c, value, possibleValues);

                r = -1;
                c = 0;
                break;
            }
        }
    }
}

void removeValueFromRows(int c, int value, std::map<int, NumberSet>& possibleValues) {
    for (int r = 0; r <= ROWS; r++) {
        if (possibleValues.count(c + r * COLS) == 0) continue;
        possibleValues[c + r * COLS].remove(value);
    }
}

void removeValueFromCols(int r, int value, std::map<int, NumberSet>& possibleValues) {
    for (int c = 0; c <= COLS; c++) {
        if (possibleValues.count(c + r * COLS) == 0) continue;
        possibleValues[c + r * COLS].remove(value);
    }
}

void removeValueFromRegion(int r, int c, int value, std::map<int, NumberSet>& possibleValues) {
    int startRow = (r / REGION_ROWS) * REGION_ROWS;
    int startCol = (c / REGION_COLS) * REGION_COLS;

    for (int r1 = startRow; r1 < startRow + REGION_ROWS; r1++) {
        for (int c1 = startCol; c1 < startCol + REGION_COLS; c1++) {
            if (possibleValues.count(c1 + r1 * COLS) == 0) continue;
            possibleValues[c1 + r1 * COLS].remove(value);
        }
    }
}
