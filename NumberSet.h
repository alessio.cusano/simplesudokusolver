#pragma once
#include <vector>
#include <string>

class NumberSet
{
private:
	std::vector<int> m_Digits;
	int m_Min;
	int m_Max;

public:
	NumberSet();
	NumberSet(int min, int max);
	NumberSet(int min, int max, std::vector<int> digits);
	NumberSet(const NumberSet& other);
	void add(int digit);
	void remove(int digit);
	bool contains(int digit) const;
	NumberSet getSpecular() const;
	std::string print() const;
	
	int size() const;

	int& operator[](int idx);
	const int& operator[](int idx) const;
	std::vector<int> operator+(const std::vector<int>& other);
	NumberSet operator+(const NumberSet& other);

private:
	int indexOf(int digit) const;
};

